# Environment - Repository

Environment creation of a central repository to manage snapshot builds, releases & environment baselines.

Thanks for the packer scripting goes to shiguredo-packer-templates.

## OBSOLETE : No longer maintained

As an individual instance was no longer part of a cohesive build pipeline, this
will no longer be maintained.

It has been superseded by [environment.buildfarm](https://bitbucket.org/n_piper/environment.buildfarm).

# Features & Overview

## Connectivity Requirments

* Jenkins build box(es) can connect (securely) and publish snapshot,releases
* Development servers can pull down dependencies via maven, etc; locally
* Ideally an aliased URL / statically named like 'repostiory.solveapuzzledev'
* Docker/Rancher can pull down dependencies
* Packer, Docker, Vagrant can add snapshots, releases 
* Database schemas?

## Storage requirements
* The path for storing the artifacts should be persistent, if possible recover from a storage facility / public repo (Dropbox, AWS, github, bitbucket,..)
* Automated backup may be required / preferred as it is running
* If vbox, VM snapshots are required storage growth may increase

## Security
* Public/Private keys - effort vs. management?
* Integration to Vault / Keystore?
* Only an Admin & read only user to start

# Proposed implementation
* Packer
* Vagrant
* Recover storage backup as repository?
* Docker?? 



# Installation

## Pre-Requisites (version tested on MacOSX Yosemite 10.10)

* Packer (0.9.0)
* Vagrant (1.7.4)
* Oracle VirtualBox (5.0.14)

## To build for Oracle 
```
$ packer build -only=virtualbox-iso template.json
$ vagrant box add ubuntu-14.04.box ubuntu-14-04-x64-virtualbox.box 
$ vagrant up
```

# Post build
http://localhost:8889/

# References
[Vagrant and Puppet envs](http://blog.kloudless.com/2013/07/01/automating-development-environments-with-vagrant-and-puppet/)

[Install JDK 8 Ubuntu silently](http://javaguru.fi/unattended-java-install-ubuntu-14-04.html)

[Artifactory storage backup](https://www.jfrog.com/confluence/display/RTF/Managing+Backups)

[Artifactory - changing default storage](https://www.jfrog.com/confluence/display/RTF/Changing+the+Default+Storage)

[Artifactory - Import & Export](https://www.jfrog.com/confluence/display/RTF/Importing+and+Exporting#ImportingandExporting-SystemImportandExport)
